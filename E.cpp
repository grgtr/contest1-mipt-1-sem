#include <algorithm>
#include <iostream>
class Queue {
 private:
  class Stack {
   private:
    int Peek_;
    int Capacity_;

   public:
    int* arr;
    int* minarr = (int*)malloc(1 * sizeof(int));

    Stack() {
      arr = (int*)malloc(1 * sizeof(int));
      Capacity_ = 1;
      Peek_ = -1;
    }

    void Push(int x) {
      arr = (int*)realloc(arr, (Size() + 1) * sizeof(int));
      minarr = (int*)realloc(minarr, (Size() + 1) * sizeof(int));
      arr[++Peek_] = x;
      if (Peek_ == 0) {
        minarr[0] = x;
      } else {
        if (minarr[Peek_ - 1] > x) {
          minarr[Peek_] = x;
        } else {
          minarr[Peek_] = minarr[Peek_ - 1];
        }
      }
    }

    int Pop() {
      int ans = Top();
      if (Size() > 1) {
        arr = (int*)realloc(arr, (Size() - 1) * sizeof(int));
        minarr = (int*)realloc(minarr, (Size() - 1) * sizeof(int));

      } else {
        arr = (int*)realloc(arr, (1) * sizeof(int));
        minarr = (int*)realloc(minarr, (1) * sizeof(int));
      }
      Peek_--;
      return ans;
    }

    int Top() { return arr[Peek_]; }

    int Size() { return Peek_ + 1; }

    bool IsEmpty() { return Size() == 0; }

    bool IsFull() { return Size() == Capacity_; }

    void Clear() {
      while (Size() > 0) {
        Peek_--;
      }
      arr = (int*)realloc(arr, (1) * sizeof(int));
      std::cout << "ok\n";
    }

    int Min() { return minarr[Peek_]; }

    ~Stack() {
      free(arr);
      free(minarr);
    };
  };

 public:
  Stack add, del;
  Queue();
  ~Queue();

  void Push(int x);
  int Pop();
  int Front();
  int Size();
  int Min();
  void Clear();
};

Queue::Queue() = default;

Queue::~Queue() = default;

void Queue::Clear() {
  while (Size() > 0) {
    Pop();
  }
}
int Queue::Size() { return add.Size() + del.Size(); }

void Queue::Push(int x) { add.Push(x); }

int Queue::Pop() {
  if (del.IsEmpty()) {
    while (!add.IsEmpty()) {
      del.Push(add.Pop());
    }
  }
  return del.Pop();
}

int Queue::Front() {
  if (del.IsEmpty()) {
    while (!add.IsEmpty()) {
      del.Push(add.Pop());
    }
  }
  return del.Top();
}

int Queue::Min() {
  if (not add.IsEmpty() and not del.IsEmpty()) {
    return add.Min() < del.Min() ? add.Min() : del.Min();
  }
  if (add.IsEmpty()) {
    return del.Min();
  }

  return add.Min();
}

void Request(Queue& qu, const std::string& str) {
  int tmp;
  if (str == "enqueue") {
    std::cin >> tmp;
    qu.Push(tmp);
    std::cout << "ok\n";
  } else if (str == "dequeue") {
    if (!qu.del.IsEmpty() or !qu.add.IsEmpty()) {
      std::cout << qu.Pop() << '\n';
    } else {
      std::cout << "error\n";
    }
  } else if (str == "front") {
    if (qu.Size() > 0) {
      std::cout << qu.Front() << '\n';
    } else {
      std::cout << "error\n";
    }
  } else if (str == "size") {
    std::cout << qu.Size() << '\n';
  } else if (str == "min") {
    if (qu.Size() > 0) {
      std::cout << qu.Min() << '\n';
    } else {
      std::cout << "error\n";
    }
  } else if (str == "clear") {
    qu.Clear();
    std::cout << "ok\n";
  }
}

int main() {
  Queue qu;
  int n;
  std::cin >> n;
  for (int j = 0; j < n; ++j) {
    std::string str;
    std::cin >> str;
    Request(qu, str);
  }
}
