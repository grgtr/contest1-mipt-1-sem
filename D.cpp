#include <deque>
#include <iostream>

void Request(char priority, std::deque<int>& first_part_queue,
             std::deque<int>& last_part_queue) {
  int goblin;
  if (priority == '*') {
    std::cin >> goblin;
    last_part_queue.push_front(goblin);
  } else if (priority == '+') {
    std::cin >> goblin;
    last_part_queue.push_back(goblin);
  } else {
    std::cout << first_part_queue.front() << std::endl;
    first_part_queue.pop_front();
  }

  if (first_part_queue.size() < last_part_queue.size()) {
    first_part_queue.push_back(last_part_queue.front());
    last_part_queue.pop_front();
  }
}

int main() {
  std::deque<int> first_part_queue, last_part_queue;
  int n;
  std::cin >> n;
  for (int i = 0; i < n; ++i) {
    char priority;
    std::cin >> priority;
    Request(priority, first_part_queue, last_part_queue);
  }
}
