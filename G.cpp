#include <cmath>
#include <iomanip>
#include <iostream>
void Fill(double*& arr, double*& multi, const int& n) {
  multi[0] = 0;
  for (int i = 0; i < n; ++i) {
    std::cin >> arr[i];
    multi[i + 1] = multi[i] + log(arr[i]);
  }
}

void Request(double*& multi) {
  int l, r;
  std::cin >> l >> r;
  std::cout << std::fixed << std::setprecision(10)
            << exp((multi[r + 1] - multi[l]) / (r - l + 1)) << std::endl;
}

int main() {
  int n, q;
  std::cin >> n;
  auto* arr = new double[n];
  auto* multi = new double[n + 1];

  Fill(arr, multi, n);
  std::cin >> q;

  for (int i = 0; i < q; ++i) {
    Request(multi);
  }

  delete[] arr;
  delete[] multi;
}
